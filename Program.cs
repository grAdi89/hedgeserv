﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HedgeServ
{
    class Program
    {
        public static void Main(string[] args)
        {
            static void FirstFourPerfectNumbers()
            {
                var divisors = new List<int>();
                var perfectNumbers = new List<int>();
                int number = 2;

                while (true)
                {
                    for (int i = number / 2; i > 0; i--)
                    {
                        if(number % i == 0)
                        {
                            divisors.Add(i);
                        }
                    }

                    if(divisors.Sum() == number)
                    {
                        perfectNumbers.Add(number);
                    }

                    divisors.Clear();

                    if(perfectNumbers.Count == 4)
                    {
                        break;
                    }
                    number++;
                }

                Console.WriteLine(string.Join(" ", perfectNumbers));
            }

            FirstFourPerfectNumbers();
        }
    }
}
